//
//  Label.cpp
//  oo_ep1
//
//  Created by Gabriel Bezerra Valério on 25/04/17.
//  Copyright © 2017 Gabriel Bezerra Valério. All rights reserved.
//

#ifndef _LABEL_HPP_
#define _LABEL_HPP_

#include "GameObject.hpp"

class Label : public GameObject {
private:
    char *text;
    
    void createFrame();
public:
    Label(char *txt, Position pos);
    ~Label();
    void setText(char *text);
    char * getText();
};

#endif
