//
//  Map.cpp
//  oo_ep1
//
//  Created by Gabriel Bezerra Valério on 23/04/17.
//  Copyright © 2017 Gabriel Bezerra Valério. All rights reserved.
//

#ifndef _MAP_HPP_
#define _MAP_HPP_

#include "GameObject.hpp"
#include "MapTile.hpp"
#include <fstream>

class Map : public GameObject {
private:
    Position playerSpawnLocation;
    MapTile **tiles;
    
    void loadMap(const char *fileName);
    TileType getSpecificMapTileTypeFor(char loaded);
public:
    Map(const char *fileName, GSize size);
    ~Map();
    
    void setTiles(MapTile **til);
    MapTile **getTiles();
    MapTile tileAt(int i, int j);
    void changeTileAt(int i, int j, MapTile tile);
    Position getPlayerSpawnLocation();
};

#endif
