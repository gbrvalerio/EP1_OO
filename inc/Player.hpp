//
//  Player.hpp
//  oo_ep1
//
//  Created by Gabriel Bezerra Valério on 25/04/17.
//  Copyright © 2017 Gabriel Bezerra Valério. All rights reserved.
//

#ifndef _PLAYER_HPP_
#define _PLAYER_HPP_

#include "GameObject.hpp"

class Player : public GameObject {
private:
    int life;

public:
    Player(Position spawn);
    
    void sumToLife(int sum);
    int getLife();
    void moveTo(Position pos);
};

#endif
