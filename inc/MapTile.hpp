//
//  MapTile.hpp
//  oo_ep1
//
//  Created by Gabriel Bezerra Valério on 23/04/17.
//  Copyright © 2017 Gabriel Bezerra Valério. All rights reserved.
//

#ifndef _MAPTILE_HPP_
#define _MAPTILE_HPP_

typedef enum tile_type {
    WALL_TILE = 0,
    FLOOR_TILE,
    BONUS_TILE,
    TRAP_TILE,
    SPAWN_TILE,
    END_TILE
} TileType;

class MapTile{
private:
    TileType type;
    char sprite;
public:
    MapTile(TileType startType);
    void setType(TileType newType);
    TileType getType();
    char getSprite();
};

#endif
