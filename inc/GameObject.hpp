#ifndef _GAMEOBJECT_HPP_
#define _GAMEOBJECT_HPP_

#include <cstdlib>

typedef struct pos {
	int x;
	int y;
} Position;

typedef struct siz {
	int width;
	int height;
} GSize;

typedef struct pix {
	char data;
	int color;
} Pixel;

class GameObject {
protected:
	Position position;
	GSize size;
	Pixel **pixels;
public:
	GameObject();
	~GameObject();
	void setPosition(Position pos);
	void setGSize(GSize siz);
	void setPixels(Pixel **pixs, GSize newSize);
	Position getPosition();
	GSize getSize();
	Pixel **getPixels();
	Pixel pixelAt(int i, int j);
    void changePixelAt(int i, int j, Pixel px);
};

#endif
