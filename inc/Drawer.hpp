//
//  Drawer.cpp
//  oo_ep1
//
//  Created by Gabriel Bezerra Valério on 23/04/17.
//  Copyright © 2017 Gabriel Bezerra Valério. All rights reserved.
//

#ifndef _DRAWER_HPP_
#define _DRAWER_HPP_

#include "GameObject.hpp"
#include <vector>
#include <ncurses.h>

class Drawer {
private:
    std::vector<GameObject *> toDraw;
    GSize bounds;
public:
    Drawer(GSize bounds);
    ~Drawer();
    void addObject(GameObject *obj);
    GameObject * removeObject(GameObject *obj);
    void draw();
    void clearScreen();
};

#endif
