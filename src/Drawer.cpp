//
//  Drawer.cpp
//  oo_ep1
//
//  Created by Gabriel Bezerra Valério on 23/04/17.
//  Copyright © 2017 Gabriel Bezerra Valério. All rights reserved.
//

#include "Drawer.hpp"

Drawer::Drawer(GSize bounds) {
    this->bounds = bounds;
    toDraw = std::vector<GameObject *>();
}

Drawer::~Drawer() {
    for(GameObject *obj : toDraw){
        delete obj;
    }
}

void Drawer::addObject(GameObject *obj) {
    toDraw.push_back(obj);
}

GameObject * Drawer::removeObject(GameObject *obj) {
    
    return NULL;
}

void Drawer::draw() {
    this->clearScreen();
    
    for(GameObject *obj : toDraw){
        GSize objSize = obj->getSize();
        Position objPosition = obj->getPosition();
        
        int maxX = (objPosition.x + objSize.width) > bounds.width ? bounds.width : (objPosition.x + objSize.width);
        int maxY = (objPosition.y + objSize.height) > bounds.height ? bounds.height : (objPosition.y + objSize.height);
        
//        printw("oPx: %d oPy: %d oSw: %d oSh: %d bW: %d bH: %d mX: %d mY: %d", objPosition.x, objPosition.y, objSize.width, objSize.height, bounds.width, bounds.height, maxX, maxY);
//        getch();
        
        int i = 0, j = 0;
        for(int y = objPosition.y; y < maxY; y++){
            for(int x = objPosition.x; x < maxX; x++){
                mvprintw(y, x, "%c", obj->pixelAt(j, i++));
            }
            i = 0;
            j++;
        }
        refresh();
    }
    
    move(bounds.height, bounds.width);
    
}

void Drawer::clearScreen() {
    clear();
}

