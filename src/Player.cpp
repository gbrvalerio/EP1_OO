//
//  Player.cpp
//  oo_ep1
//
//  Created by Gabriel Bezerra Valério on 25/04/17.
//  Copyright © 2017 Gabriel Bezerra Valério. All rights reserved.
//

#include "Player.hpp"

Player::Player(Position spawn) : GameObject() {
    Pixel **px = (Pixel **) malloc(sizeof(Pixel *));
    
    if(px == NULL)
        throw "Erro ao alocar memoria para a aplicacao! Player::Player 1";
        
    px[0] = (Pixel *) malloc(sizeof(Pixel));
    
    if(px[0] == NULL)
        throw "Erro ao alocar memoria para a aplicacao! Player::Player 2";
    
    Pixel p;
    p.data = 'o';
    p.color = 2;
    
    px[0][0] = p;
    
    GSize sz;
    sz.width = 1;
    sz.height = 1;
    
    this->setPixels(px, sz);
    this->setPosition(spawn);
    
    this->life = 100;
}

void Player::sumToLife(int sum){
    this->life += sum;
}

int Player::getLife(){
    return this->life;
}

void Player::moveTo(Position pos){
    this->setPosition(pos);
}
