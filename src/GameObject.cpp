#include "GameObject.hpp"

GameObject::GameObject(){
	size.width = 1;
	size.height = 1;
	position.x = 0;
	position.y = 0;

	pixels = (Pixel **) malloc(sizeof(Pixel *));

	if(pixels == NULL)
		throw "Erro ao alocar memoria.";
	pixels[0] = (Pixel *) malloc(sizeof(Pixel));
	if(pixels[0] == NULL)
		throw "Erro ao alocar memoria.";
	pixels[0][0].data = ' ';
	pixels[0][0].color = 0;
}

GameObject::~GameObject(){
	for(int i = 0; i < size.height; i++){
		free(pixels[i]);
	}
	free(pixels);
}

void GameObject::setPosition(Position pos){
	if(pos.x < 0 || pos.y < 0)
		throw "A posicao nao pode ser menor que zero!";

	this->position = pos;
}

void GameObject::setGSize(GSize siz){
	if(siz.width < 0 || siz.height < 0)
		throw "O tamanho nao pode ser menor que zero!";

	this->size = siz;
}

void GameObject::setPixels(Pixel **pixs, GSize newSize){
    if(newSize.width < 0 || newSize.height < 0)
        throw "Width e Height nao podem ser menores que zero!";
    
//    for(int i = 0; i < size.height; i++){
//        free(pixels[i]);
//    }
//    free(pixels);
    
    pixels = pixs;
    
    this->setGSize(newSize);
}

Position GameObject::getPosition(){
	return this->position;
}

GSize GameObject::getSize(){
	return this->size;
}

Pixel **GameObject::getPixels(){
	return this->pixels;
}

Pixel GameObject::pixelAt(int i, int j){
	return this->pixels[i][j];
}

void GameObject::changePixelAt(int i, int j, Pixel px){
    this->pixels[i][j] = px;
}
