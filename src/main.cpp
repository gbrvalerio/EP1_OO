#include <iostream>
#include <ncurses.h>
#include "Map.hpp"
#include "Drawer.hpp"
#include "Label.hpp"
#include "Player.hpp"

#define ERR_COLOR 0

bool canMoveTo(Player *player, Map *map, Position toPos);

int main(int argc, char ** argv){
    int row, col;
    
    initscr();
    // resizeterm(34, 74);
    noecho();
    start_color();
    init_pair(ERR_COLOR, COLOR_RED, COLOR_BLACK);
    getmaxyx(stdscr, row, col);
    
    if(row < 34 || col < 74){
        attron(COLOR_PAIR(ERR_COLOR));
        mvprintw(row/2, col/2 - 19, "Resize your terminal to at least 34x74!");
        getch();
        endwin();
        return 0;
    }
    
    GSize mapSize;
    mapSize.width = 70;
    mapSize.height = 30;
    Map *map = new Map("level1.txt", mapSize);
    
    Position p;
    p.x = 1;
    p.y = 31;
    Label *label = new Label((char *) "Life: ", p);
    
    Player *player = new Player(map->getPlayerSpawnLocation());
    
    GSize bounds;
    bounds.width = 74;
    bounds.height = 34;
    Drawer *drawer = new Drawer(bounds);
    
    drawer->addObject(map);
    drawer->addObject(label);
    drawer->addObject(player);
    
    mvprintw(row/2, col/2 - 25, "Use WASD to move or press X to exit at any moment!");
    getch();
    
    char input = ' ';
    do {
        Position playerPos = player->getPosition();
        Position auxP = playerPos;
        switch (input) {
            case 'w':
                auxP.y--;
                if(canMoveTo(player, map, auxP)){
                    playerPos.y--;
                }
                break;
            case 'a':
                auxP.x--;
                if(canMoveTo(player, map, auxP)){
                    playerPos.x--;
                }
                break;
            case 's':
                auxP.y++;
                if(canMoveTo(player, map, auxP)){
                    playerPos.y++;
                }
                break;
            case 'd':
                auxP.x++;
                if(canMoveTo(player, map, auxP)){
                    playerPos.x++;
                }
                break;
            default:
                break;
        }
        player->moveTo(playerPos);
        
        drawer->draw();
        
        mvprintw(31, 7, "%d  ", player->getLife());
        
        if(player->getLife() == 0){
            clear();
            refresh();
            
            mvprintw(row / 2, col / 2 - 4, "YOU LOST!");
            getch();
            break;
        }
    } while((input = getch()) != 'x');
    
    
    clear();
    refresh();
    
    
    delete drawer;
    endwin();
    
    return 0;
}

bool canMoveTo(Player *player, Map *map, Position toPos){
    TileType type = map->tileAt(toPos.y, toPos.x).getType();
    Pixel p;
    p.data = ' ';
    p.color = 1;
    MapTile tile(FLOOR_TILE);
    switch(type){
        case WALL_TILE:
            return false;
        case BONUS_TILE:
            player->sumToLife(10);
            map->changePixelAt(toPos.y, toPos.x, p);
            map->changeTileAt(toPos.y, toPos.x, tile);
            break;
        case TRAP_TILE:
            player->sumToLife(-10);
            map->changePixelAt(toPos.y, toPos.x, p);
            map->changeTileAt(toPos.y, toPos.x, tile);
            break;
        default:
            break;
    }
    
    return true;
}
