//
//  Label.cpp
//  oo_ep1
//
//  Created by Gabriel Bezerra Valério on 25/04/17.
//  Copyright © 2017 Gabriel Bezerra Valério. All rights reserved.
//

#include "Label.hpp"
#include <string.h>

Label::Label(char *txt, Position pos) : GameObject(){
    this->setPosition(pos);
    
    this->text = txt;
    
    this->createFrame();
}

Label::~Label(){
    
}

void Label::setText(char *text){
    this->text = text;
    
    this->createFrame();
}

char * Label::getText(){
    return this->text;
}

void Label::createFrame(){
    size_t strSize = strlen(this->text);
    GSize sz;
    sz.width = (int) strSize;
    sz.height = 1;
    
    Pixel **px = (Pixel **) malloc(sizeof(Pixel *));
    
    if(px == NULL)
        throw "Erro ao alocar memoria para a aplicacao! Label::createFrame 1";
    
    px[0] = (Pixel *) malloc(sizeof(Pixel) * strSize);
    
    if(px[0] == NULL)
        throw "Erro ao alocar memoria para a aplicacao! Label::createFrame 2";
    
    for(int i = 0; i < strSize; i++){
        Pixel p;
        p.data = this->text[i];
        p.color = 0;
        
        px[0][i] = p;
    }
    
    this->setPixels(px, sz);
}
