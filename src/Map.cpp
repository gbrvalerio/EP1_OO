//
//  Map.cpp
//  oo_ep1
//
//  Created by Gabriel Bezerra Valério on 23/04/17.
//  Copyright © 2017 Gabriel Bezerra Valério. All rights reserved.
//

#include "Map.hpp"
#include <ncurses.h>

#define WALL '='
#define TRAP '$'
#define BONUS '#'
#define GROUND ' '
#define PLAYER 's'

Map::Map(const char *fileName, GSize size) : GameObject() {
    this->setGSize(size);
    
    Position p;
    p.x = p.y = 0;
    this->setPosition(p);
    
    this->loadMap(fileName);
}

void Map::loadMap(const char *fileName){
    std::ifstream mapFile;
    
    mapFile.open(std::string(fileName), std::ios::in);
    
    if(mapFile.is_open()){
        Pixel **pix = (Pixel **) malloc(sizeof(Pixel *) * size.height);
        MapTile **til = (MapTile **) malloc(sizeof(MapTile *) * size.height);
        if(pix == NULL || til == NULL)
            throw "Erro ao alocar memoria para a aplicacao! Map::loadMap 1";
        
        std::string line;
        int row = 0;
        while(getline(mapFile, line)){
            pix[row] = (Pixel *) malloc(sizeof(Pixel) * size.width);
            til[row] = (MapTile *) malloc(sizeof(MapTile) * size.width);
            
            if(pix[row] == NULL || til[row] == NULL)
                throw "Erro ao alocar memoria para a aplicacao! Map::loadMap 2";
            
            for(int i = 0; i < size.width; i++){
                TileType type = this->getSpecificMapTileTypeFor(line.c_str()[i]);
                
                Pixel pixel;
                
                MapTile tile = MapTile(type);
                pixel.data = tile.getSprite();
                pixel.color = 0;
                
                pix[row][i] = pixel;
                til[row][i] = tile;
                
                if(type == SPAWN_TILE){
                    playerSpawnLocation.x = i;
                    playerSpawnLocation.y = row;
                }
            }
            row++;
        }
        
        this->setTiles(til);
        this->setPixels(pix, size);
        
        mapFile.close();
    } else {
        throw("Erro ao abrir mapa! Arquivo nao pode ser carregado!");
    }
}

TileType Map::getSpecificMapTileTypeFor(char loaded){
    switch(loaded){
        case WALL:
            return WALL_TILE;
        case TRAP:
            return TRAP_TILE;
        case BONUS:
            return BONUS_TILE;
        case PLAYER:
            return SPAWN_TILE;
        default:
            return FLOOR_TILE;
    }
}

Position Map::getPlayerSpawnLocation(){
    return this->playerSpawnLocation;
}

void Map::setTiles(MapTile **til){
    this->tiles = til;
}

MapTile **Map::getTiles(){
    return this->tiles;
}

MapTile Map::tileAt(int i, int j){
    return this->tiles[i][j];
}

void Map::changeTileAt(int i, int j, MapTile tile){
    this->tiles[i][j] = tile;
}
