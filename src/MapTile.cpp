//
//  MapTile.cpp
//  oo_ep1
//
//  Created by Gabriel Bezerra Valério on 23/04/17.
//  Copyright © 2017 Gabriel Bezerra Valério. All rights reserved.
//

#include "MapTile.hpp"

MapTile::MapTile(TileType startType){
    this->type = startType;
    
    switch(type){
        case WALL_TILE:
            this->sprite = '#';
            break;
        case BONUS_TILE:
            this->sprite = '$';
            break;
        case TRAP_TILE:
            this->sprite = '@';
            break;
        default:
            this->sprite = ' ';
            break;
    }
}

void MapTile::setType(TileType newType){
    this->type = newType;
}

TileType MapTile::getType(){
    return this->type;
}

char MapTile::getSprite(){
    return this->sprite;
}
