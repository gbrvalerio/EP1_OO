CC := g++
CFLAGS := -std=c++11 -Wall -lncurses
SRCFILES := $(wildcard src/*.cpp)
all: $(SRCFILES:src/%.cpp=obj/%.o)
	$(CC) $(CFLAGS) obj/*.o -o bin/saida 
obj/%.o : src/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@ -I./inc
.PHONY: clean
clean: 
	rm -rf obj/*
	rm -rf bin/*
run:
	bin/saida
